import experimentBuilder as bld
from experimentBuilder.experiment import Experiment
import time

import matplotlib.pyplot as plt

NAME = "co"
WIDTH = 5
HEIGTH = 5
ROBOTS_NUMBER = 100
RUNS_IN_PACK = 50
FILE = None #"./experiments/exp_colors.json"

if FILE == None:
    start_time = time.time()
    exp = bld.Experiment(NAME, " Изменение эффективности РРТС при влиянии ВР при увеличении количества цветов (2-5)")

    pack_2 = bld.RunPack(
        white=50, black=50, red=0, green=0, blue=0,
        width=WIDTH, height=HEIGTH,
        robots_number=ROBOTS_NUMBER,
        comment="2 цвета"
    )
    pack_2.build_xml([bld.robots(WIDTH, HEIGTH, ROBOTS_NUMBER), bld.build_wall(WIDTH, HEIGTH)], visualization=False)
    pack_2.run(RUNS_IN_PACK)
    exp.add(pack_2)

    pack_3 = bld.RunPack(
        white=33.33, black=33.33, red=33.33, green=0, blue=0,
        width=WIDTH, height=HEIGTH,
        robots_number=ROBOTS_NUMBER,
        comment="3 цвета"
    )
    pack_3.build_xml([bld.robots(WIDTH, HEIGTH, ROBOTS_NUMBER), bld.build_wall(WIDTH, HEIGTH)], visualization=False)
    pack_3.run(RUNS_IN_PACK)
    exp.add(pack_3)

    pack_4 = bld.RunPack(
        white=25, black=25, red=25, green=25, blue=0,
        width=WIDTH, height=HEIGTH,
        robots_number=ROBOTS_NUMBER,
        comment="4 цвета"
    )
    pack_4.build_xml([bld.robots(WIDTH, HEIGTH, ROBOTS_NUMBER), bld.build_wall(WIDTH, HEIGTH)], visualization=False)
    pack_4.run(RUNS_IN_PACK)
    exp.add(pack_4)   
    
    pack_5 = bld.RunPack(
        white=20, black=20, red=20, green=20, blue=20,
        width=WIDTH, height=HEIGTH,
        robots_number=ROBOTS_NUMBER,
        comment="5 цветов"
    )
    pack_5.build_xml([bld.robots(WIDTH, HEIGTH, ROBOTS_NUMBER), bld.build_wall(WIDTH, HEIGTH)], visualization=False)
    pack_5.run(RUNS_IN_PACK)
    exp.add(pack_5)

    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = Experiment.load(FILE)

# from tkinter import Tk, font
# root = Tk()
# print(font.families())

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Время достижения консенсуса
data_1 = []
for pack in exp.packs:
    buf = []
    for run in pack.runs:
        buf.append(run.time/10)
    data_1.append(buf)

color = "black"
markersize=1

fig1, ax1 = plt.subplots()
ax = plt.gca()
#ax.set_ylim(0,400) # Минимальная-максимальная высота
#ax1.set_title("Заголовок")
ax1.set_xlabel("C")
ax1.set_ylabel(r"$\mathrm{E_N}$")
ax1.boxplot(data_1, labels=[2, 3, 4, 5], boxprops=dict(color=color),
            capprops=dict(color=color),
            whiskerprops=dict(color=color),
            flierprops=dict(color=color, markeredgecolor=color, markersize=markersize),
            medianprops=dict(color=color),)
plt.savefig(f"./experiments/exp_{NAME}_1.png", bbox_inches='tight')
plt.show()


############# Процент консесуса на белом цвете
data_2 = []
names_2 = ["2", "3", "4", "5"]
for pack in exp.packs:
    whites = 0
    for run in pack.runs:
        if run.max_color == "white":
            whites += 1
    data_2.append(whites/len(pack.runs))

fig1, ax1 = plt.subplots()
ax = plt.gca()
ax.set_ylim(0, 1)  # Минимальная-максимальная высота
#ax1.set_title(bunch.name)
ax1.set_xlabel("C")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
ax1.bar(names_2, data_2)
plt.savefig(f"./experiments/exp_{NAME}_2.png", bbox_inches='tight')
plt.show()

