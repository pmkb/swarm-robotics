import experimentBuilder as bld
import matplotlib.pyplot as plt

NEW = True

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "area-byz_random_(DL_20_0-20_2-6_2_0.85)"
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 100
BYZ_COLORS = 5
WHITE = 40;    BLACK = 15;    RED = 15;      GREEN = 15;    BLUE = 15
COMMUNICATION_RANGE = 0.22
AREAS = [1, 2]

if NEW:
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии ВР при изменении размера исследуемой внешней среды")
    
    for i in AREAS:
        data = []
        for j in range(ROBOTS_NUMBER+1):
            pack = bld.RunPack(
                white=WHITE, black=BLACK, red=RED, green=GREEN, blue=BLUE,
                width=i, height=i,
                robots=ROBOTS_NUMBER-j, robots_communication_range=COMMUNICATION_RANGE,
                byz_random_robots=j, byz_random_colors=BYZ_COLORS, byz_random_communication_range=COMMUNICATION_RANGE,
                comment=f"сторона {i} м, {j} византийских роботов"
            )
            pack.build_xml(visualization=False)
            pack.run(RUNS_IN_PACK)
            data.append(pack)
        exp.packs[i] = data

    exp.finish()
    exp.save()
    print(f"--- {exp.duration/60} mins ---")

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Рисуем графики
x = range(0, ROBOTS_NUMBER+1)

time = {}
accuracy = {}

for key, value in exp.packs.items():
    time_buf = []
    accuracy_buf = []
    for puck in value:
        time_buf.append(puck.mean_step)
        accuracy_buf.append(puck.white_run / len(puck.runs))
    time[key] = time_buf
    accuracy[key] = accuracy_buf

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(20, 9))

ax1.set_xticks(x)
ax1.set_xlabel("BR")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
for key, value in time.items():
    label = round( (int(key)*int(key)) / (ROBOTS_NUMBER * 3.14 * COMMUNICATION_RANGE * COMMUNICATION_RANGE), 2)
    print(label)
    ax1.plot(x, value, "o-", label=f"M={label}")
ax1.legend(loc="upper right")

ax2.set_xticks(x)
ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
ax2.set_xlabel("BR")
ax2.set_ylabel(r"$\mathrm{E_N}$")
for key, value in accuracy.items():
    label = round( (int(key)*int(key)) / (ROBOTS_NUMBER * 3.14 * COMMUNICATION_RANGE * COMMUNICATION_RANGE), 2)
    ax2.plot(x, value, "o-", label=f"M={label}")
ax2.legend(loc="upper right")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()
