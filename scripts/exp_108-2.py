from numpy import append, mean
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt
from experimentBuilder.util import calculateColor
from tqdm import tqdm
import pandas as pd

NEW = False

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "108_2_(DL_20_0-10_5_5_0.85)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 100
BYZ = ("sect", "rand", "en", "sect_reejct", "rand_reejct", "en_reejct")
RANGE = 10
COLORS_NUMBER = 5
COLORS_DIFFICULT = 0.85
MAX_TIME = 0

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии различных ВР")

    main_c, other_c = calculateColor(COLORS_NUMBER, COLORS_DIFFICULT)
    other_c2 = other_c3 = other_c4 = other_c5 = 0
    if COLORS_NUMBER >= 2: other_c2 = other_c
    if COLORS_NUMBER >= 3: other_c3 = other_c
    if COLORS_NUMBER >= 4: other_c4 = other_c
    if COLORS_NUMBER >= 5: other_c5 = other_c
    print("COLORS")
    print(f"white={main_c}, other={other_c}, colors_number={COLORS_NUMBER}, colors_difficult={COLORS_DIFFICULT}")

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=False,
            robots=ROBOTS_NUMBER-i,
            byz_sectarian_robots=i,
            comment=f"{i} роботов сектантов, reject_votes=False",
            run_length = MAX_TIME
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[0]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=False,
            robots=ROBOTS_NUMBER-i,
            byz_random_robots=i, byz_random_colors=COLORS_NUMBER,
            comment=f"{i} случайных роботов, reject_votes=False",
            run_length = MAX_TIME
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[1]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=False,
            robots=ROBOTS_NUMBER-i,
            byz_enemy_robots=i,
            comment=f"{i} роботов врагов, reject_votes=False",
            run_length = MAX_TIME
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[2]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=True,
            robots=ROBOTS_NUMBER-i,
            byz_sectarian_robots=i,
            comment=f"{i} роботов сектантов, reject_votes=True",
            run_length = MAX_TIME
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[3]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=True,
            robots=ROBOTS_NUMBER-i,
            byz_random_robots=i, byz_random_colors=COLORS_NUMBER,
            comment=f"{i} случайных роботов, reject_votes=True",
            run_length = MAX_TIME
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[4]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=True,
            robots=ROBOTS_NUMBER-i,
            byz_enemy_robots=i,
            comment=f"{i} роботов врагов, reject_votes=True",
            run_length = MAX_TIME
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[5]] = pack



    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

x = range(0, RANGE)
xx = []
for t in x:
    if t % 2 == 0:
        xx.append(str(t))
    else:
        xx.append("")

time = {}
accuracy = {}

for name in BYZ:
    time_buf = []
    accuracy_buf = []
    for pack in exp.packs[name]:
        buf = []
        for run in pack.runs:
            if run.consensus_reached:
                buf.append(run.step)
        time_buf.append(buf)
        if pack.consensus_reached_number == 0:
            accuracy_buf.append(0)
        else:
            accuracy_buf.append(pack.white_run / pack.consensus_reached_number)
    time[name] = time_buf
    accuracy[name] = accuracy_buf


font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(nrows=3, ncols=2, figsize=(15, 15))

color = "black"
markersize=1.5
color1 = "blue"
color2 = (1, 0.5, 0, 1)

axs = {BYZ[0]: [ax1, ax2], BYZ[1]: [ax3, ax4], BYZ[2]: [ax5, ax6]}

for name in [BYZ[0], BYZ[1], BYZ[2]]:
    xx1 = []
    xx1_5 = []
    xx2 = []

    for t in range(0, RANGE*3):
        if t % 3 == 0:
            xx1.append(t)
            xx1_5.append(t+0.5)
        if (t+2) % 3 == 0:
            xx2.append(t)

    name_2 = name + "_reejct"

    axs[name][0].set_title(name)
    axs[name][0].set_ylim(0,7000) # Минимальная-максимальная высота
    axs[name][0].set_xlabel("Количество ВР")
    axs[name][0].set_ylabel(r"$\mathrm{T^{correct}_N}$" )

    axs[name][0].boxplot(time[name], positions = xx1, boxprops=dict(color=color1, linewidth=1.5),
                capprops=dict(color=color1, linewidth=1.5),
                whiskerprops=dict(color=color1, linewidth=1.5),
                flierprops=dict(color=color1, markeredgecolor=color1, markersize=markersize),
                medianprops=dict(color=color1, linewidth=1.5), widths = 0.8)
    axs[name][0].boxplot(time[name_2], positions = xx2, boxprops=dict(color=color2, linewidth=1.5),
            capprops=dict(color=color2, linewidth=1.5),
            whiskerprops=dict(color=color2, linewidth=1.5),
            flierprops=dict(color=color2, markeredgecolor=color2, markersize=markersize),
            medianprops=dict(color=color2, linewidth=1.5), widths = 0.8)
    
    axs[name][0].set_xticks(xx1_5)
    axs[name][0].set_xticklabels(xx)

    axs[name][1].set_title(name)
    axs[name][1].set_ylim(0, 1)  # Минимальная-максимальная высота
    #axs[name][1].set_xlim(-0.5,12) # Минимальная-максимальная высота.
    axs[name][1].set_xlabel("Количество ВР")
    axs[name][1].set_ylabel(r"$\mathrm{E_N}$")
    
    axs[name][1].bar(xx1, accuracy[name], color=color1, label=f"Прототип", width=1)
    axs[name][1].bar(xx2, accuracy[name_2], color=color2, label=f"Предлагаемый метод", width=1)
    axs[name][1].set_xticks(xx1_5)
    axs[name][1].set_xticklabels(xx)
    #axs[name][1].legend(fontsize = 10, title_fontsize = '10' )



axs[BYZ[0]][1].legend(fontsize = 10, title_fontsize = '10' )
map = {}
for name in BYZ:
    buf = []
    for arr in time[name]:
        buf.append(mean(arr))
    map[f"{name}_time"] = buf
    map[f"{name}_accuracy"] = accuracy[name]
data = pd.DataFrame(map)
data.to_csv(f"./experiments/exp_{NAME}.csv")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()