from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

from experimentBuilder.util import calculateColor

NEW = True

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "101_sect_(DL_20_0-20_2_2_0.65-0.85)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 100
COLOR_COMPLEXITYS = [0.45, 0.65, 0.85]

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии ВР на разных сложностях.")
    
    for j in COLOR_COMPLEXITYS:
        main_c, other_c = calculateColor(5, j)
        print(main_c, other_c)

        pack = []
        for i in range(ROBOTS_NUMBER+1):
            pack_2 = bld.RunPack(
                white=main_c, black=other_c, red=other_c, green=other_c, blue=other_c,
                width=WIDTH, height=HEIGTH,
                robots=ROBOTS_NUMBER-i,
                byz_sectarian_robots=i,
                comment=f"5 цветов, {i} роботов сектантов, {j} сложность"
            )
            pack_2.build_xml(visualization=False)
            pack_2.run(RUNS_IN_PACK)

            pack.append(pack_2)
        exp.packs[j] = pack

    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

x = range(0, 10+1)
xx = []
for t in x:
    if t % 2 == 0:
        xx.append(str(t))
    else:
        xx.append("")

time = {}
accuracy = {}

for name in COLOR_COMPLEXITYS:
    time_buf = []
    accuracy_buf = []
    for pack in exp.packs[name]:
        buf = []
        for run in pack.runs:
            if run.consensus_reached:
                buf.append(run.step)
        time_buf.append(buf)
        accuracy_buf.append(pack.white_run / len(pack.runs))
    time[name] = time_buf[0:11]
    accuracy[name] = accuracy_buf[0:11]


font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(nrows=3, ncols=2, figsize=(15, 15))

color = "black"
markersize=1

axs = {0.45: [ax1, ax2], 0.65: [ax3, ax4], 0.85: [ax5, ax6]}

for name in COLOR_COMPLEXITYS:
    axs[name][0].set_ylim(0,1000) # Минимальная-максимальная высота
    #axs[name][0].set_xlim(0.5,12) # Минимальная-максимальная высота
    axs[name][0].set_xlabel("BR")
    axs[name][0].set_ylabel(r"$\mathrm{T^{correct}_N}$" )
    axs[name][0].boxplot(time[name], labels=xx, boxprops=dict(color=color),
                capprops=dict(color=color),
                whiskerprops=dict(color=color),
                flierprops=dict(color=color, markeredgecolor=color, markersize=markersize),
                medianprops=dict(color=color))

    axs[name][1].set_ylim(0, 1)  # Минимальная-максимальная высота
    #axs[name][1].set_xlim(-0.5,12) # Минимальная-максимальная высота
    axs[name][1].set_xlabel("BR")
    axs[name][1].set_ylabel(r"$\mathrm{E_N}$")
    axs[name][1].bar(x , accuracy[name])

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()