from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt
import pandas as pd
from collections import OrderedDict

from experimentBuilder.util import calculateColor


# метод_количество-роботов-всего_колво-ВР_размер-поля_колво-цветов_сложность 
NAME = "107-6-3_sect_(DL_20_0-20_5_5_*)"
WIDTH = 5
HEIGTH = 5
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 1
CONSENSUS_MIN_STEP = 5000
COLORS_NUMBER = 5

start_time = time.time()
exp = bld.Experiment(NAME, "Тестирование уверенность различных типов роботов")

pack_2 = bld.RunPack(
    white=40, black=15, red=15, green=15, blue=15,
    width=WIDTH, height=HEIGTH,
    byz_sectarian_robots=1,
    byz_enemy_robots=1, 
    byz_random_robots=1, byz_random_colors=COLORS_NUMBER, byz_random_colors_timer=1000,
    robots=ROBOTS_NUMBER-3,
    comment=f"TEST",
    consensus_min_step = CONSENSUS_MIN_STEP,
    full_log="robot"
)
pack_2.build_xml(visualization=False)
pack_2.run(RUNS_IN_PACK)

exp.save()
print("--- %s seconds ---" % (time.time() - start_time))

data = pd.read_csv("temp/full_log.csv", delimiter=";", index_col = ["step", "robot_id"])
colums = data.columns.tolist()
for i in range(ROBOTS_NUMBER):
    row_max = data[[colums[0 + 5*i], colums[1 + 5*i], colums[2 + 5*i], colums[3 + 5*i], colums[4 + 5*i]]].max(axis=1, skipna=True)
    data[f"rob_{i}"] = (data.iloc[:, 0 + 5*i] + data.iloc[:, 1 + 5*i] + data.iloc[:, 2 + 5*i] + data.iloc[:, 3 + 5*i] + data.iloc[:, 4 + 5*i] - row_max) / row_max / (COLORS_NUMBER - 1)


font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)
fig1, axs = plt.subplots(nrows=1, ncols=1, figsize=(20, 10))

data_slice = data.loc[(slice(None), "ep0"), :]
for i in range(ROBOTS_NUMBER):
    if (colums[5*i]).startswith("byz_enemy"):
        data_slice[f"rob_{i}"].plot.line(ax=axs, color='red', label=f"Враг")
    elif (colums[5*i]).startswith("byz_random"):
        data_slice[f"rob_{i}"].plot.line(ax=axs, color='purple', label=f"Случайный")
    elif (colums[5*i]).startswith("byz_sect"):
        data_slice[f"rob_{i}"].plot.line(ax=axs, color='orange', label=f"Сектант")
    else:
        data_slice[f"rob_{i}"].plot.line(ax=axs, color='green', label=f"Обычный")
handles, labels = plt.gca().get_legend_handles_labels()
by_label = OrderedDict(zip(labels, handles))
plt.legend(by_label.values(), by_label.keys())

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
data.to_csv(f"./experiments/exp_{NAME}.csv")
plt.show()