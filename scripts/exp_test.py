import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

NEW = True

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "test"
WIDTH = 2
HEIGTH = 2
NORMAL_ROBOTS_NUBER = 10
BYZ_ROBOTS_NUBER = 10
RUNS_IN_PACK = 1
COMMUNICATION_RANGE = 0.22
MAX_LENGTH = 500
COLORS = [66, 34, 0, 0, 0]

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии византийских врагов при увеличении количества признаков внешней среды (1-5).")
    
    pack = bld.RunPack(
        white=COLORS[0], black=COLORS[1], red=COLORS[2], green=COLORS[3], blue=COLORS[4],
        width=WIDTH, height=HEIGTH,
        reject_votes_vor_all=True,
        robots=NORMAL_ROBOTS_NUBER,
        byz_sectarian_robots=BYZ_ROBOTS_NUBER,
        #byz_sectarian_robots=BYZ_ROBOTS_NUBER, byz_sectarian_communication_range=COMMUNICATION_RANGE,
        comment=f"цвета: {COLORS}, {BYZ_ROBOTS_NUBER} враждебных византийских роботов",
        consensus_min_step = 50,
        run_length = MAX_LENGTH
    )
    pack.build_xml(visualization=True)
    pack.run(RUNS_IN_PACK)

    exp.finish()
    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Рисуем графики
x = range(0, BYZ_ROBOTS_NUBER+1)
time = {}
accuracy = {}

for color in range(len(COLORS)):
    time_buf = []
    accuracy_buf = []
    for pack in exp.packs[color]:
        time_buf.append(pack.mean_step)
        accuracy_buf.append(pack.white_run / pack.consensus_reached_number)
    time[color] = time_buf
    accuracy[color] = accuracy_buf

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(20, 9))

ax1.set_xticks(x)
ax1.set_xlabel("BR")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
for color in range(len(COLORS)):
    ax1.plot(x, time[color], "o-", label=f"{color+1}")
ax1.legend(loc="upper right")

ax2.set_xticks(x)
ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
ax2.set_xlabel("BR")
ax2.set_ylabel(r"$\mathrm{E_N}$")
for color in range(len(COLORS)):
    ax2.plot(x, accuracy[color], "o-", label=f"{color+1}")
ax2.legend(loc="upper right")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()
