from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt
from experimentBuilder.util import calculateColor
from tqdm import tqdm

NEW = False

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "108_(DL_20_0-20_2_2_*)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 100
BYZ = ("sect", "rand", "en", "sect_reejct", "rand_reejct", "en_reejct")
RANGE = 10
COLORS_NUMBER = 2
COLORS_DIFFICULT = 0.85

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии различных ВР")

    main_c, other_c = calculateColor(COLORS_NUMBER, COLORS_DIFFICULT)
    other_c2 = other_c3 = other_c4 = other_c5 = 0
    if COLORS_NUMBER >= 2: other_c2 = other_c
    if COLORS_NUMBER >= 3: other_c3 = other_c
    if COLORS_NUMBER >= 4: other_c4 = other_c
    if COLORS_NUMBER >= 5: other_c5 = other_c
    print("COLORS")
    print(f"white={main_c}, other={other_c}, colors_number={COLORS_NUMBER}, colors_difficult={COLORS_DIFFICULT}")

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=False,
            robots=ROBOTS_NUMBER-i,
            byz_sectarian_robots=i,
            comment=f"{i} роботов сектантов, reject_votes=False",
            run_length = 100
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[0]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=False,
            robots=ROBOTS_NUMBER-i,
            byz_random_robots=i, byz_random_colors=COLORS_NUMBER,
            comment=f"{i} случайных роботов, reject_votes=False",
            run_length = 100
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[1]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=False,
            robots=ROBOTS_NUMBER-i,
            byz_enemy_robots=i,
            comment=f"{i} роботов врагов, reject_votes=False",
            run_length = 100
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[2]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=True,
            robots=ROBOTS_NUMBER-i,
            byz_sectarian_robots=i,
            comment=f"{i} роботов сектантов, reject_votes=True",
            run_length = 100
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[3]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=True,
            robots=ROBOTS_NUMBER-i,
            byz_random_robots=i, byz_random_colors=COLORS_NUMBER,
            comment=f"{i} случайных роботов, reject_votes=True",
            run_length = 100
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[4]] = pack

    pack = []
    for i in tqdm(range(RANGE)):
        pack_2 = bld.RunPack(
            white=main_c, black=other_c2, red=other_c3, green=other_c4, blue=other_c5,
            width=WIDTH, height=HEIGTH,
            reject_votes_vor_all=True,
            robots=ROBOTS_NUMBER-i,
            byz_enemy_robots=i,
            comment=f"{i} роботов врагов, reject_votes=True",
            run_length = 100
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK, print_result=False)

        pack.append(pack_2)
    exp.packs[BYZ[5]] = pack



    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

x = range(0, RANGE)
xx = []
for t in x:
    if t % 2 == 0:
        xx.append(str(t))
    else:
        xx.append("")

time = {}
accuracy = {}

for name in BYZ:
    time_buf = []
    accuracy_buf = []
    for pack in exp.packs[name]:
        buf = []
        for run in pack.runs:
            if run.consensus_reached:
                buf.append(run.step)
        time_buf.append(buf)
        if pack.consensus_reached_number == 0:
            accuracy_buf.append(0)
        else:
            accuracy_buf.append(pack.white_run / pack.consensus_reached_number)
    time[name] = time_buf
    accuracy[name] = accuracy_buf


font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8), (ax9, ax10), (ax11, ax12)) = plt.subplots(nrows=6, ncols=2, figsize=(15, 30))

color = "black"
markersize=1

axs = {BYZ[0]: [ax1, ax2], BYZ[1]: [ax3, ax4], BYZ[2]: [ax5, ax6], BYZ[3]: [ax7, ax8], BYZ[4]: [ax9, ax10], BYZ[5]: [ax11, ax12]}

for name in BYZ:
    axs[name][0].set_title(name)
    axs[name][0].set_ylim(0,1000) # Минимальная-максимальная высота
    #axs[name][0].set_xlim(0.5,12) # Минимальная-максимальная высота
    axs[name][0].set_xlabel("BR")
    axs[name][0].set_ylabel(r"$\mathrm{T^{correct}_N}$" )
    axs[name][0].boxplot(time[name], labels=xx, boxprops=dict(color=color),
                capprops=dict(color=color),
                whiskerprops=dict(color=color),
                flierprops=dict(color=color, markeredgecolor=color, markersize=markersize),
                medianprops=dict(color=color))
    print(time[name])

    axs[name][1].set_ylim(0, 1)  # Минимальная-максимальная высота
    #axs[name][1].set_xlim(-0.5,12) # Минимальная-максимальная высота
    axs[name][1].set_xlabel("BR")
    axs[name][1].set_ylabel(r"$\mathrm{E_N}$")
    axs[name][1].bar(x , accuracy[name])

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()