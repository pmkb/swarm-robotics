N_COLORS = 5
COMPL = 0.65


k = COMPL * (N_COLORS-1)
x = (100 * k) / (1 + k)
y = 100 - x

main = round(y, 2)
other = round(x/(N_COLORS-1), 2)

print(N_COLORS, f"main color={main}", f"other={other}*{N_COLORS-1}")