import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

NEW = False

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "colors-byz_random-easy_(DL_20_0-20_2_2-5_0.45)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 100
WHITE_2 = 68.97;    BLACK_2 = 31.03
WHITE_3 = 52.63;    BLACK_3 = 23.69;    RED_3 = 23.69
WHITE_4 = 42.55;    BLACK_4 = 19.15;    RED_4 = 19.15;      GREEN_4 = 19.15
WHITE_5 = 35.71;    BLACK_5 = 16.07;    RED_5 = 16.07;      GREEN_5 = 16.07;    BLUE_5 = 16.07

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии ВР при увеличении количества признаков внешней среды (2-5).")
    
    for i in range(ROBOTS_NUMBER+1):
        pack_2 = bld.RunPack(
            white=WHITE_2, black=BLACK_2, red=0, green=0, blue=0,
            width=WIDTH, height=HEIGTH,
            robots_number=ROBOTS_NUMBER-i, 
            byz_random_robots=i, byz_random_colors_number=2,
            comment=f"2 цвета, {i} византийских роботов"
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK)
        exp.packs.append(pack_2)

    for i in range(ROBOTS_NUMBER+1):
        pack_3 = bld.RunPack(
            white=WHITE_3, black=BLACK_3, red=RED_3, green=0, blue=0,
            width=WIDTH, height=HEIGTH,
            robots_number=ROBOTS_NUMBER-i, 
            byz_random_robots=i, byz_random_colors_number=3,
            comment=f"3 цвета, {i} византийских роботов"
        )
        pack_3.build_xml(visualization=False)
        pack_3.run(RUNS_IN_PACK)
        exp.packs.append(pack_3)

    for i in range(ROBOTS_NUMBER+1):
        pack_4 = bld.RunPack(
            white=WHITE_4, black=BLACK_4, red=RED_4, green=GREEN_4, blue=0,
            width=WIDTH, height=HEIGTH,
            robots_number=ROBOTS_NUMBER-i, 
            byz_random_robots=i, byz_random_colors_number=4,
            comment=f"4 цвета, {i} византийских роботов"
        )
        pack_4.build_xml(visualization=False)
        pack_4.run(RUNS_IN_PACK)
        exp.packs.append(pack_4) 

    for i in range(ROBOTS_NUMBER+1):
        pack_5 = bld.RunPack(
            white=WHITE_5, black=BLACK_5, red=RED_5, green=GREEN_5, blue=BLUE_5,
            width=WIDTH, height=HEIGTH,
            robots_number=ROBOTS_NUMBER-i, 
            byz_random_robots=i, byz_random_colors_number=5,
            comment=f"5 цветов, {i} византийских роботов"
        )
        pack_5.build_xml(visualization=False)
        pack_5.run(RUNS_IN_PACK)
        exp.packs.append(pack_5) 

    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Рисуем графики
x = range(0, ROBOTS_NUMBER+1)

time_2 = []
time_3 = []
time_4 = []
time_5 = []

accuracy_2 = []
accuracy_3 = []
accuracy_4 = []
accuracy_5 = []

for i in range((ROBOTS_NUMBER+1)):
    time_2.append(exp.packs[i].mean_step)
    accuracy_2.append(exp.packs[i].white_run / len(exp.packs[i].runs))
for i in range((ROBOTS_NUMBER+1)*1, (ROBOTS_NUMBER+1)*2):
    time_3.append(exp.packs[i].mean_step)
    accuracy_3.append(exp.packs[i].white_run / len(exp.packs[i].runs))
for i in range((ROBOTS_NUMBER+1)*2, (ROBOTS_NUMBER+1)*3):
    time_4.append(exp.packs[i].mean_step)
    accuracy_4.append(exp.packs[i].white_run / len(exp.packs[i].runs))
for i in range((ROBOTS_NUMBER+1)*3, (ROBOTS_NUMBER+1)*4):
    time_5.append(exp.packs[i].mean_step)
    accuracy_5.append(exp.packs[i].white_run / len(exp.packs[i].runs))

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(20, 9))

ax1.set_xticks(x)
ax1.set_xlabel("BR")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
ax1.plot(x, time_2, "o-", label="C=2")
ax1.plot(x, time_3, "o-", label="C=3")
ax1.plot(x, time_4, "o-", label="C=4")
ax1.plot(x, time_5, "o-", label="C=5")
ax1.legend(loc="upper right")

ax2.set_xticks(x)
ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
ax2.set_xlabel("BR")
ax2.set_ylabel(r"$\mathrm{E_N}$")
ax2.plot(x, accuracy_2, "o-", label="C=2")
ax2.plot(x, accuracy_3, "o-", label="C=3")
ax2.plot(x, accuracy_4, "o-", label="C=4")
ax2.plot(x, accuracy_5, "o-", label="C=5")
ax2.legend(loc="upper right")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()
