from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

NEW = True

RAND_NAME = "RAND"
ENEMY_NAME = "ENEMY"
SECT_NAME = "SECT"
NAMES = [RAND_NAME, ENEMY_NAME, SECT_NAME]

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "byz-all_(DL_20_0-20_4_4_0.85)"
WIDTH = 4
HEIGTH = 4
ROBOTS_NUMBER = 20
MAX_BYZ_NUMBER = 10
RUNS_IN_PACK = 100
WHITE = 22.73;    BLACK = 19.32;    RED = 19.32;      GREEN = 19.32;    BLUE = 19.32
#WHITE = 35.71;    BLACK = 16.07;    RED = 16.07;      GREEN = 16.07;    BLUE = 16.07
COMMUNICATION_RANGE = 0.22
MAX_LENGTH = 500

BYZ_RANDOM_COLORS = 5

if NEW:
    exp = bld.Experiment(NAME, "Тестирование различных византийских роботов")
    
    rand_pack = []
    for i in range(MAX_BYZ_NUMBER+1):
        pack_2 = bld.RunPack(
            white=WHITE, black=BLACK, red=RED, green=GREEN, blue=BLUE,
            width=WIDTH, height=HEIGTH,
            robots=ROBOTS_NUMBER-i, robots_communication_range=COMMUNICATION_RANGE,
            byz_random_robots=i, byz_random_colors=BYZ_RANDOM_COLORS, byz_random_communication_range=COMMUNICATION_RANGE,
            comment=f"{i} случайных византийских роботов",
            run_length = MAX_LENGTH
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK)
        rand_pack.append(pack_2)
    exp.packs[RAND_NAME] = rand_pack

    enemy_pack = []
    for i in range(MAX_BYZ_NUMBER+1):
        pack_2 = bld.RunPack(
            white=WHITE, black=BLACK, red=RED, green=GREEN, blue=BLUE,
            width=WIDTH, height=HEIGTH,
            robots=ROBOTS_NUMBER-i, robots_communication_range=COMMUNICATION_RANGE,
            byz_enemy_robots=i, byz_enemy_communication_range=COMMUNICATION_RANGE,
            comment=f"{i} враждебных византийских роботов",
            run_length = MAX_LENGTH
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK)
        enemy_pack.append(pack_2)
    exp.packs[ENEMY_NAME] = enemy_pack

    sect_pack = []
    for i in range(MAX_BYZ_NUMBER+1):
        pack_2 = bld.RunPack(
            white=WHITE, black=BLACK, red=RED, green=GREEN, blue=BLUE,
            width=WIDTH, height=HEIGTH,
            robots=ROBOTS_NUMBER-i, robots_communication_range=COMMUNICATION_RANGE,
            byz_sectarian_robots=i, byz_sectarian_communication_range=COMMUNICATION_RANGE,
            comment=f"{i} сектантских византийских роботов",
            run_length = MAX_LENGTH
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK)
        sect_pack.append(pack_2)
    exp.packs[SECT_NAME] = sect_pack

    exp.finish()
    exp.save()
    print(f"--- {exp.duration/60} mins ---")

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Рисуем графики
x = range(0, MAX_BYZ_NUMBER+1)
xx = []
for t in x:
    if t % 5 == 0:
        xx.append(str(t))
    else:
        xx.append("")

time = {}
accuracy = {}

for name in NAMES:
    time_buf = []
    accuracy_buf = []
    for pack in exp.packs[name]:
        buf = []
        for run in pack.runs:
            if run.consensus_reached:
                buf.append(run.step)
        time_buf.append(buf)
        accuracy_buf.append(pack.white_run / len(pack.runs))
    time[name] = time_buf
    accuracy[name] = accuracy_buf


font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(nrows=3, ncols=2, figsize=(15, 15))

color = "black"
markersize=1

axs = {RAND_NAME: [ax1, ax2], ENEMY_NAME: [ax3, ax4], SECT_NAME: [ax5, ax6]}

for name in NAMES:
    axs[name][0].set_ylim(0,2000) # Минимальная-максимальная высота
    axs[name][0].set_xlabel("BR")
    axs[name][0].set_ylabel(r"$\mathrm{T^{correct}_N}$" )
    axs[name][0].boxplot(time[name], labels=xx, boxprops=dict(color=color),
                capprops=dict(color=color),
                whiskerprops=dict(color=color),
                flierprops=dict(color=color, markeredgecolor=color, markersize=markersize),
                medianprops=dict(color=color))

    axs[name][1].set_ylim(0, 1)  # Минимальная-максимальная высота
    axs[name][1].set_xlabel("BR")
    axs[name][1].set_ylabel(r"$\mathrm{E_N}$")
    axs[name][1].bar(x , accuracy[name])

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()