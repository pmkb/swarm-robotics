from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt
import pandas as pd

from experimentBuilder.util import calculateColor


# метод_количество-роботов-всего_колво-ВР_размер-поля_колво-цветов_сложность 
NAME = "106-1_sect_(DL_20_0-20_2_2_1)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 1
CONSENSUS_MIN_STEP = 5000
COLORS_NUMBER = 2

start_time = time.time()
exp = bld.Experiment(NAME, "Тестирование уверенность различных типов роботов")

pack_2 = bld.RunPack(
    white=75, black=25, red=0, green=0, blue=0,
    width=WIDTH, height=HEIGTH,
    byz_sectarian_robots=1,
    byz_enemy_robots=1, 
    byz_random_robots=1, byz_random_colors=COLORS_NUMBER, byz_random_colors_timer=1000,
    robots=ROBOTS_NUMBER-3,
    comment=f"TEST",
    consensus_min_step = CONSENSUS_MIN_STEP,
    full_log="robot"
)
pack_2.build_xml(visualization=False)
pack_2.run(RUNS_IN_PACK)

exp.save()
print("--- %s seconds ---" % (time.time() - start_time))

data = pd.read_csv("temp/full_log.csv", delimiter=";", index_col = ["step", "robot_id"])
colums = data.columns.tolist()
for i in range(ROBOTS_NUMBER):
    row_max = data[[colums[0 + 5*i], colums[1 + 5*i], colums[2 + 5*i], colums[3 + 5*i], colums[4 + 5*i]]].max(axis=1, skipna=True)
    data[f"rob_{i}"] = (data.iloc[:, 0 + 5*i] + data.iloc[:, 1 + 5*i] + data.iloc[:, 2 + 5*i] + data.iloc[:, 3 + 5*i] + data.iloc[:, 4 + 5*i] - row_max) / row_max / (COLORS_NUMBER - 1)

#print(data)

# print(data)
# print(
#     data.loc[
#         (slice(None), 'ep0'),
#         :
#     ]
# )

# df = pd.DataFrame(columns=list)
# # print(df)
# # print(data.loc[1, "byz_enemy_ep0"])
# # print(data.loc[2])
# # print(data.loc[3])
# for i in range(1, 1000):
#     df.append(data.loc[i, "byz_enemy_ep0"])
# #print(df)

print(data)



# x = range(0, 10)

# time = {}
# accuracy = {}

# for key, value in exp.packs.items():
#     time_buf = []
#     accuracy_buf = []
#     for puck in value[0:len(x)]:
#         time_buf.append(puck.mean_step)
#         accuracy_buf.append(puck.white_run / len(puck.runs))
#     time[key] = time_buf
#     accuracy[key] = accuracy_buf

# font = {
#     'family': "Arial",
#     'size': 14
#     }

# plt.rc('font', **font)
fig1, axs = plt.subplots(nrows=5, ncols=4, figsize=(80, 20))

robots = []
for i in range(ROBOTS_NUMBER):
    robots.append(data.index[i][1])


for x in range(4):
    for y in range(5):
        data_slice = data.loc[(slice(None), robots[x*5 + y]), :]
        for i in range(ROBOTS_NUMBER):
            if (colums[5*i]).startswith("byz_enemy"):
                data_slice[f"rob_{i}"].plot.line(ax=axs[y][x], color='red')
            elif (colums[5*i]).startswith("byz_random"):
                data_slice[f"rob_{i}"].plot.line(ax=axs[y][x], color='purple')
            elif (colums[5*i]).startswith("byz_sect"):
                data_slice[f"rob_{i}"].plot.line(ax=axs[y][x], color='orange')
            else:
                data_slice[f"rob_{i}"].plot.line(ax=axs[y][x], color='green')

# # ax1.set_xticks(x)
# # ax1.set_xlabel("BR")
# # ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
# # for key, value in time.items():
# #     ax1.plot(x, value, "o-", label=f"M={key}")
# # ax1.legend(loc="upper right")

# # ax2.set_xticks(x)
# # ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
# # ax2.set_xlabel("BR")
# # ax2.set_ylabel(r"$\mathrm{E_N}$")
# # for key, value in accuracy.items():
# #     ax2.plot(x, value, "o-", label=f"M={key}")
# # ax2.legend(loc="upper right")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()
data.to_csv("test")