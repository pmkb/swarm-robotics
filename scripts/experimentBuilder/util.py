
def calculateColor(n_colors, compl):
    k = compl * (n_colors-1)
    x = (100 * k) / (1 + k)
    y = 100 - x

    main = round(y, 2)
    other = round(x/(n_colors-1), 2)

    #print(n_colors, f"main color={main}", f"other={other}*{n_colors-1}")

    return main, other

    
