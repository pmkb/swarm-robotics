
def build_wall(x_size: int, y_size: int, width: float = 0.01) -> str:
    return f"""
    <box id="wall_north" size="{width},{x_size + width},0.05" movable="false">
        <body position="{y_size},{x_size/2},0" orientation="0,0,0" />
    </box>
    <box id="wall_south" size="{width},{x_size + width},0.05" movable="false">
        <body position="0,{x_size/2},0" orientation="0,0,0" />
    </box>
        <box id="wall_west" size="{y_size + width},{width},0.05" movable="false">
        <body position="{y_size/2},{x_size},0" orientation="0,0,0" />
    </box>
    <box id="wall_east" size="{y_size + width},{width},0.05" movable="false">
        <body position="{y_size/2},0,0" orientation="0,0,0" />
    </box>
    """.strip()
    
def robots(x_size: int, y_size: int, quantity: int) -> str:
    return f"""
        <distribute>
            <position method="uniform" min="0.1,0.1,0" max="{y_size-0.1}, {x_size-0.1}, 0"/>
            <orientation method="uniform" min="0,0,0" max="360, 0, 0"/>
            <entity quantity="{quantity}" max_trials="20">
                <e-puck id="ep">
                    <controller config="fdc"/>
                </e-puck>
            </entity>
        </distribute>
    """.strip()

def byz_random_robots(x_size: int, y_size: int, quantity: int) -> str:
    return f"""
        <distribute>
            <position method="uniform" min="0.1,0.1,0" max="{y_size-0.1}, {x_size-0.1}, 0"/>
            <orientation method="uniform" min="0,0,0" max="360, 0, 0"/>
            <entity quantity="{quantity}" max_trials="20">
                <e-puck id="byz_random_ep">
                    <controller config="byz_random"/>
                </e-puck>
            </entity>
        </distribute>
    """.strip()

def byz_enemy_robots(x_size: int, y_size: int, quantity: int) -> str:
    return f"""
        <distribute>
            <position method="uniform" min="0.1,0.1,0" max="{y_size-0.1}, {x_size-0.1}, 0"/>
            <orientation method="uniform" min="0,0,0" max="360, 0, 0"/>
            <entity quantity="{quantity}" max_trials="20">
                <e-puck id="byz_enemy_ep">
                    <controller config="byz_enemy"/>
                </e-puck>
            </entity>
        </distribute>
    """.strip()

def byz_sectarian_robots(x_size: int, y_size: int, quantity: int) -> str:
    return f"""
        <distribute>
            <position method="uniform" min="0.1,0.1,0" max="{y_size-0.1}, {x_size-0.1}, 0"/>
            <orientation method="uniform" min="0,0,0" max="360, 0, 0"/>
            <entity quantity="{quantity}" max_trials="20">
                <e-puck id="byz_sect_ep">
                    <controller config="byz_sect"/>
                </e-puck>
            </entity>
        </distribute>
    """.strip()