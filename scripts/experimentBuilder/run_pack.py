from typing import List
import subprocess
import pathlib

from experimentBuilder.builder import build_visualization
import experimentBuilder as bld


class RunPack:
    def __init__(self,
                white: float, black: float, red: float, green: float, blue: float,
                width: int, height: int,
                comment: str,
                arena_wall: bool = True,
                reject_votes_vor_all = False,
                robots: int = 20, robots_communication_range = 0.22,
                byz_random_robots: int = 0, byz_random_colors: int = 5, byz_random_communication_range = 0.22, 
                    byz_random_colors_timer = 1,
                byz_enemy_robots: int = 0, byz_enemy_communication_range = 0.22,
                byz_sectarian_robots: int = 0, byz_sectarian_communication_range = 0.22,
                run_length = 0, consensus_min_step = 1, full_log = "NONE",
                floor_type = "random_shuffle"
                ):
                
        self.white = white
        self.black = black
        self.red = red
        self.green = green
        self.blue = blue

        self.width = width
        self.height = height
        self.arena_wall = arena_wall

        if reject_votes_vor_all:
            self.reject_votes_vor_all = "true"
        else:
            self.reject_votes_vor_all = "false"

        self.robots = robots
        self.robots_communication_range = robots_communication_range

        self.byz_random_robots = byz_random_robots
        self.byz_random_colors = byz_random_colors
        self.byz_random_communication_range = byz_random_communication_range
        self.byz_random_colors_timer = byz_random_colors_timer

        self.byz_enemy_robots = byz_enemy_robots
        self.byz_enemy_communication_range = byz_enemy_communication_range

        self.byz_sectarian_robots = byz_sectarian_robots
        self.byz_sectarian_communication_range = byz_sectarian_communication_range

        self.run_length = run_length
        self.consensus_min_step = consensus_min_step
        self.full_log = full_log
        self.floor_type = floor_type
        self.comment = comment

        self.consensus_reached_number = 0
        self.mean_step = 0
        self.white_run = 0
        self.black_run = 0
        self.red_run = 0
        self.green_run = 0
        self.blue_run = 0
        self.orange_run = 0

        self.argox_xml = ""
        self.runs: List[bld.Run] = []
        

    def __str__(self) -> str:
        return f"RunPack(white={self.white}, black={self.black}, red={self.red}, green={self.green}, blue={self.blue})"


    def calculate(self):
        self.consensus_reached_number = 0
        self.mean_step = 0
        self.white_run = 0
        self.black_run = 0
        self.red_run = 0
        self.green_run = 0
        self.blue_run = 0
        self.orange_run = 0

        for run in self.runs:
            if run.consensus_reached:
                self.consensus_reached_number += 1
                self.mean_step += run.step

                if run.max_color == "white":
                    self.white_run += 1
                elif run.max_color == "black":
                    self.black_run += 1
                elif run.max_color == "red":
                    self.red_run += 1
                elif run.max_color == "green":
                    self.green_run += 1
                elif run.max_color == "blue":
                    self.blue_run += 1
                elif run.max_color == "orange":
                    self.orange_run += 1
        
        if self.consensus_reached_number != 0:
            self.mean_step /= self.consensus_reached_number

    def run(self, times: int = 1, print_result = True):
        for i in range(times):
            file_path = "temp/output.argos"
            text_file = open(file_path, "w")
            text_file.write(self.argox_xml)
            text_file.close()

            file = pathlib.Path(file_path).absolute()

            if print_result:
                result = subprocess.run(["argos3", "-c", file])
                print(result)
            else:
                subprocess.run(["argos3", "-c", file], stdout=subprocess.PIPE)

            run = bld.Run.parse()
            self.runs.append(run)
        self.calculate()

    def build_xml(self, visualization: bool = False, seed: int = 0):
        vis = ""
        if visualization:
            vis = build_visualization()

        allEntities = ""
        if self.arena_wall:
            allEntities += bld.build_wall(self.width, self.height)
        allEntities += bld.byz_random_robots(self.width, self.height, self.byz_random_robots)
        allEntities += bld.robots(self.width, self.height, self.robots)
        allEntities += bld.byz_enemy_robots(self.width, self.height, self.byz_enemy_robots)
        allEntities += bld.byz_sectarian_robots(self.width, self.height, self.byz_sectarian_robots)

        self.argox_xml = f"""
    <?xml version="1.0" ?>
    <argos-configuration>
        
        <physics_engines>
            <dynamics2d id="dyn2d" />
        </physics_engines>

        <media>
            <range_and_bearing id="rab"/>
            <led id="leds"/>
        </media>

        <framework>
            <system threads="0"/>
            <experiment length="{self.run_length}" ticks_per_second="10" random_seed="{seed}" />
        </framework>

        <visualization>
            {vis}
        </visualization>

        <loop_functions library="build/loop_functions/main_loop/libmain_loop" label="main_loop">
            <params 
                whitePercent={self.white}
                blackPercent={self.black}
                redPercent={self.red}
                greenPercent={self.green}
                bluePercent={self.blue}
                floorType={self.floor_type}
                fullLog={self.full_log}
                consensusMinStep={self.consensus_min_step}
            />
        </loop_functions>

        <controllers>
            <default_controller id="fdc" library="build/controllers/epuck/libepuck_accumulation">
                <actuators>
                    <epuck_wheels implementation="default"/>
                    <epuck_rgb_leds implementation="default" medium="leds"/>
                </actuators>
                <sensors>
                    <epuck_proximity implementation="default" show_rays="true"/>
                    <epuck_ground implementation="rot_z_only"/>
                </sensors>
                <params communicationRange="{self.robots_communication_range}" rejectVotes="{self.reject_votes_vor_all}"/>
            </default_controller>

            <byz_random_controller id="byz_random" library="build/controllers/epuck/libepuck_accumulation">
                <actuators>
                    <epuck_wheels implementation="default"/>
                    <epuck_rgb_leds implementation="default" medium="leds"/>
                </actuators>
                <sensors>
                    <epuck_proximity implementation="default" show_rays="true"/>
                    <epuck_ground implementation="rot_z_only"/>
                </sensors>
                <params colorsNumber="{self.byz_random_colors}" communicationRange="{self.byz_random_communication_range}" colorsTimer="{self.byz_random_colors_timer}" rejectVotes="{self.reject_votes_vor_all}"/>
            </byz_random_controller>

            <byz_enemy_controller id="byz_enemy" library="build/controllers/epuck/libepuck_accumulation">
                <actuators>
                    <epuck_wheels implementation="default"/>
                    <epuck_rgb_leds implementation="default" medium="leds"/>
                </actuators>
                <sensors>
                    <epuck_proximity implementation="default" show_rays="true"/>
                    <epuck_ground implementation="rot_z_only"/>
                </sensors>
                <params communicationRange="{self.byz_enemy_communication_range}" rejectVotes="{self.reject_votes_vor_all}"/>
            </byz_enemy_controller>

            <byz_sectarian_controller id="byz_sect" library="build/controllers/epuck/libepuck_accumulation">
                <actuators>
                    <epuck_wheels implementation="default"/>
                    <epuck_rgb_leds implementation="default" medium="leds"/>
                </actuators>
                <sensors>
                    <epuck_proximity implementation="default" show_rays="true"/>
                    <epuck_ground implementation="rot_z_only"/>
                </sensors>
                <params communicationRange="{self.byz_sectarian_communication_range}" rejectVotes="{self.reject_votes_vor_all}"/>
            </byz_sectarian_controller>

        </controllers>

        <arena size="{self.height}, {self.width}, 1" center="{self.height/2},{self.width/2},0">
                <floor id="floor"
                source="loop_functions"
                pixels_per_meter="50"/>

            {allEntities}
        </arena>

    </argos-configuration>
        """.strip()