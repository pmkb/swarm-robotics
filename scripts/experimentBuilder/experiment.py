import json
import os
from json import JSONEncoder
from collections import namedtuple
from typing import Any, Dict, List
import matplotlib.pyplot as plt
import time
from sys import argv
import jsonpickle


from experimentBuilder.run_pack import RunPack

class Experiment:
    def __init__(self, name: str, comment: str):

        self.name = name
        self.comment = comment
        self.start_time = time.time()
        self.duration = 0

        self.packs: Dict[Any, List[RunPack]] = {}

    def finish(self):
        self.duration =  time.time() - self.start_time

    def save(self):
        class EmployeeEncoder(JSONEncoder):
            def default(self, o):
                return o.__dict__

        #JsonData = json.dumps(self, indent=4, cls=EmployeeEncoder, ensure_ascii=False)

        jsonpickle.set_preferred_backend('json')
        jsonpickle.set_encoder_options('json', ensure_ascii=False)
        JsonData = jsonpickle.encode(self, indent=4, keys=True, numeric_keys=True)
        with open(f"experiments/exp_{self.name}.json", 'w', encoding='utf8') as json_file:
            json_file.write(JsonData)

    @staticmethod
    def load(path: str):
        with open(path, 'r',  encoding='utf8') as file:
            data = file.read()
            #return  json.loads(data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
            return jsonpickle.decode(data, keys=True)