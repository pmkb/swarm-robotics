class Run:
    def __init__(self, step: int, seed: int, max_color: str, max_color_number: int, consensus_reached: bool):
        self.step = step
        self.seed = seed
        self.max_color = max_color
        self.max_color_number = max_color_number
        self.consensus_reached: bool = consensus_reached

    @staticmethod
    def parse():
        file = "temp/result.csv"
        with open(file, 'r') as file:
            line = file.readlines()[0]
            data = line.split(";")

            step = int(data[0])
            seed = int(data[1])
            max_color = data[2]
            max_color_number = int(data[3])
            if int(data[4]) == 1:
                consensus_reached = True
            else:
                consensus_reached = False

            return Run(step, seed, max_color, max_color_number, consensus_reached)

    def __str__(self) -> str:
        return f"Run(step={self.step}, seed={self.seed}, max_color={self.max_color}, max_color_number={self.max_color_number})"