from typing import List

def build_visualization():
    return """
    <qt-opengl>
        <user_functions library="build/qt_plugin/libqt_plugin" label="qt_plugin_functions"/>
       <camera>
            <placements>
                <placement index="0" position="1,1,7" look_at="1,1,6" up="-1,0,0" lens_focal_length="65" />
            </placements>
        </camera>
    </qt-opengl>
    """.strip()