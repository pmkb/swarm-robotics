from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

from experimentBuilder.util import calculateColor

NEW = True

# метод_количество-роботов-всего_колво-ВР_размер-поля_колво-цветов_сложность 
NAME = "104-1_sect_(DL_20_0-20_2_2_1)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 100
RUNS_IN_PACK = 100
COMMUNICATION_RANGE = 0.22
ARENAS = ["random_shuffle", "random_stripes", "chess_field"]

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС на разных аренах")
    
    for j in ARENAS:
        pack = []
        for i in range(10, ROBOTS_NUMBER+1, 10):
            pack_2 = bld.RunPack(
                white=50, black=50, red=0, green=0, blue=0,
                width=2, height=2,
                robots=i,
                comment=f"{i} роботов, тип покрытия: {j}",
                floor_type = j
            )
            pack_2.build_xml(visualization=False)
            pack_2.run(RUNS_IN_PACK)

            pack.append(pack_2)
        exp.packs[j] = pack

    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")


x = range(0, 10)

time = {}
accuracy = {}

for key, value in exp.packs.items():
    time_buf = []
    accuracy_buf = []
    for puck in value[0:len(x)]:
        time_buf.append(puck.mean_step)
        accuracy_buf.append(puck.white_run / len(puck.runs))
    time[key] = time_buf
    accuracy[key] = accuracy_buf

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(20, 9))

ax1.set_xticks(x)
ax1.set_xlabel("BR")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
for key, value in time.items():
    ax1.plot(x, value, "o-", label=f"M={key}")
ax1.legend(loc="upper right")

ax2.set_xticks(x)
ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
ax2.set_xlabel("BR")
ax2.set_ylabel(r"$\mathrm{E_N}$")
for key, value in accuracy.items():
    ax2.plot(x, value, "o-", label=f"M={key}")
ax2.legend(loc="upper right")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()
