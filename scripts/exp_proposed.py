from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

NEW = True

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "proposed_(DL_20_0-20_2_2_0.8)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 100
WHITE = 55.00;    BLACK = 45.00;    RED = 00.00;    GREEN = 00.00;      BLUE = 00.00

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии ВР.")
    
    for i in range(ROBOTS_NUMBER+1):
        pack_2 = bld.RunPack(
            white=WHITE, black=BLACK, red=0, green=0, blue=0,
            width=WIDTH, height=HEIGTH,
            robots_number=ROBOTS_NUMBER-i, 
            byz_random_robots=i, byz_random_colors_number=2,
            comment=f"2 цвета, {i} византийских роботов"
        )
        pack_2.build_xml(visualization=False)
        pack_2.run(RUNS_IN_PACK)
        exp.packs.append(pack_2)

    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Рисуем графики
x = range(0, ROBOTS_NUMBER+1)
xx = []
for t in x:
    if t % 5 == 0:
        xx.append(str(t))
    else:
        xx.append("")

time = []
accuracy = []

for pack in exp.packs:
    buf = []
    for run in pack.runs:
        buf.append(run.step)
    time.append(buf)
    accuracy.append(pack.white_run / len(pack.runs))

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(15, 5))

color = "black"
markersize=1

ax1.set_ylim(0,800) # Минимальная-максимальная высота
ax1.set_xlabel("BN")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$" )
ax1.boxplot(time, labels=xx, boxprops=dict(color=color),
            capprops=dict(color=color),
            whiskerprops=dict(color=color),
            flierprops=dict(color=color, markeredgecolor=color, markersize=markersize),
            medianprops=dict(color=color))

ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
ax2.set_xlabel("BN")
ax2.set_ylabel(r"$\mathrm{E_N}$")
ax2.bar(x , accuracy)

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()