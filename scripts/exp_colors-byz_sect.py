import experimentBuilder as bld
import time
import matplotlib.pyplot as plt

NEW = True

# метод_количество роботов всего_колво ВР_размер поля_колво цветов_сложность 
NAME = "colors-byz_sect_(DL_20_0-20_2_2-5_0.45)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
MAX_BYZ_NUMBER = 10
RUNS_IN_PACK = 100
COMMUNICATION_RANGE = 0.22
MAX_LENGTH = 500
COLORS = [
    #[100.0, 00.00, 00.00, 00.00, 00.00],
    #[68.97, 31.03, 00.00, 00.00, 00.00],
    #[52.63, 23.69, 23.69, 00.00, 00.00],
    #[42.55, 19.15, 19.15, 19.15, 00.00],
    [35.71, 16.07, 16.07, 16.07, 16.07]
]

if NEW:
    start_time = time.time()
    exp = bld.Experiment(NAME, "Изменение эффективность РРТС при влиянии византийских врагов при увеличении количества признаков внешней среды (1-5).")
    
    for color in range(len(COLORS)):
        named_pack = []
        for i in range(MAX_BYZ_NUMBER+1):
            pack = bld.RunPack(
            white=COLORS[color][0], black=COLORS[color][1], red=COLORS[color][2], green=COLORS[color][3], blue=COLORS[color][4],
            width=WIDTH, height=HEIGTH,
            robots=ROBOTS_NUMBER-i, robots_communication_range=COMMUNICATION_RANGE,
            byz_sectarian_robots=i, byz_sectarian_communication_range=COMMUNICATION_RANGE,
            comment=f"цвета: {color}, {i} враждебных византийских роботов",
            run_length = MAX_LENGTH
            )
            pack.build_xml(visualization=True)
            pack.run(RUNS_IN_PACK)
            named_pack.append(pack)
        exp.packs[color] = named_pack

    exp.finish()
    exp.save()
    print("--- %s seconds ---" % (time.time() - start_time))

else: 
    exp = bld.Experiment.load(f"./experiments/exp_{NAME}.json")

font = {
    'family': "Arial",
    'size': 14}
plt.rc('font', **font)

############# Рисуем графики
x = range(0, MAX_BYZ_NUMBER+1)
time = {}
accuracy = {}

for color in range(len(COLORS)):
    time_buf = []
    accuracy_buf = []
    for pack in exp.packs[color]:
        time_buf.append(pack.mean_step)
        accuracy_buf.append(pack.white_run / pack.consensus_reached_number)
    time[color] = time_buf
    accuracy[color] = accuracy_buf

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, figsize=(20, 9))

ax1.set_xticks(x)
ax1.set_xlabel("BR")
ax1.set_ylabel(r"$\mathrm{T^{correct}_N}$")
for color in range(len(COLORS)):
    ax1.plot(x, time[color], "o-", label=f"{color+1}")
ax1.legend(loc="upper right")

ax2.set_xticks(x)
ax2.set_ylim(0, 1)  # Минимальная-максимальная высота
ax2.set_xlabel("BR")
ax2.set_ylabel(r"$\mathrm{E_N}$")
for color in range(len(COLORS)):
    ax2.plot(x, accuracy[color], "o-", label=f"{color+1}")
ax2.legend(loc="upper right")

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
plt.show()
