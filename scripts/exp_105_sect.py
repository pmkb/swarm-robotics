from numpy import append
import experimentBuilder as bld
import time
import matplotlib.pyplot as plt
import pandas as pd
from collections import OrderedDict

from experimentBuilder.util import calculateColor


# метод_количество-роботов-всего_колво-ВР_размер-поля_колво-цветов_сложность 
NAME = "105-10-3_(DL_20_20_2_2_1)"
WIDTH = 2
HEIGTH = 2
ROBOTS_NUMBER = 20
RUNS_IN_PACK = 1
CONSENSUS_MIN_STEP = 5000
COLORS_NUMBER = 5

start_time = time.time()
exp = bld.Experiment(NAME, "Тестирование уверенность различных типов роботов")

pack_2 = bld.RunPack(
    white=20, black=20, red=20, green=20, blue=20,
    width=WIDTH, height=HEIGTH,
    byz_sectarian_robots=1,
    byz_enemy_robots=1, 
    byz_random_robots=1, byz_random_colors=COLORS_NUMBER, byz_random_colors_timer=1,
    robots=ROBOTS_NUMBER-3,
    comment=f"TEST",
    consensus_min_step = CONSENSUS_MIN_STEP,
    floor_type = "random_stripes",
    full_log="observer"
)
pack_2.build_xml(visualization=False)
pack_2.run(RUNS_IN_PACK)

exp.packs["0"] = pack_2
exp.save()
print("--- %s seconds ---" % (time.time() - start_time))

data = pd.read_csv("temp/full_log.csv", delimiter=";", index_col = "step")
list = data.columns.tolist()

for i in range(ROBOTS_NUMBER):
    row_max = data[[list[0 + 5*i], list[1 + 5*i], list[2 + 5*i], list[3 + 5*i], list[4 + 5*i]]].max(axis=1)
    data[f"rob_{i}"] = (data.iloc[:, 0 + 5*i] + data.iloc[:, 1 + 5*i] + data.iloc[:, 2 + 5*i] + data.iloc[:, 3 + 5*i] + data.iloc[:, 4 + 5*i] - row_max) / row_max / (COLORS_NUMBER - 1)
#print(data)

font = {
    'family': "Arial",
    'size': 14
    }

plt.rc('font', **font)

fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=(10, 5))
for i in range(ROBOTS_NUMBER):
    if (list[5*i]).startswith("byz_enemy"):
        data[f"rob_{i}"].plot.line(ax=ax1, color='red', label=f"Враг")
    elif (list[5*i]).startswith("byz_random"):
        data[f"rob_{i}"].plot.line(ax=ax1, color='purple', label=f"Случайный")
    elif (list[5*i]).startswith("byz_sect"):
        data[f"rob_{i}"].plot.line(ax=ax1, color='orange', label=f"Сектант")
    else:
        data[f"rob_{i}"].plot.line(ax=ax1, color='green', label=f"Обычный")

handles, labels = plt.gca().get_legend_handles_labels()
by_label = OrderedDict(zip(labels, handles))
plt.legend(by_label.values(), by_label.keys())

plt.savefig(f"./experiments/exp_{NAME}.png", bbox_inches='tight')
data.to_csv(f"./experiments/exp_{NAME}.csv")
plt.show()
