#ifndef RANDOM_WALK_H
#define RANDOM_WALK_H

#include <argos3/plugins/robots/e-puck/control_interface/ci_epuck_wheels_actuator.h>
#include <argos3/plugins/robots/e-puck/control_interface/ci_epuck_proximity_sensor.h>
#include <argos3/core/utility/math/rng.h>

using namespace argos;

class RandomWalk
{
private:
    CCI_EPuckWheelsActuator* wheels; // Управление шасси
    CCI_EPuckProximitySensor* proximitySensors; // Датчики приближения
    CRandom::CRNG* randomGenerator;

    int maxStepsForWalk, maxStepsForTurn;
    int moveTime = 1;
    int direction = 1; // 0 - прямо 1 - налево 2 - направо

public:
    RandomWalk() {}
    RandomWalk(CCI_EPuckWheelsActuator* wheels, CCI_EPuckProximitySensor* proximitySensors, CRandom::CRNG* randomGenerator, int maxStepsForWalk, int maxStepsForTurn);
    ~RandomWalk();
    void move();
};

#endif