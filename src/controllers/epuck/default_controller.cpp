#include "default_controller.h"

#include <argos3/core/utility/configuration/argos_configuration.h>
#include <math.h>

void DefaultController::Init(TConfigurationNode& t_node) {
   wheels = GetActuator<CCI_EPuckWheelsActuator>("epuck_wheels");
   leds = GetActuator<CCI_EPuckRGBLEDsActuator>("epuck_rgb_leds");
   
   proximitySensors = GetSensor<CCI_EPuckProximitySensor>("epuck_proximity");
   groundSensors = GetSensor<CCI_EPuckGroundSensor>("epuck_ground");
   
   randomGenerator = CRandom::CreateRNG("argos");

   walk = RandomWalk(wheels, proximitySensors, randomGenerator, 400, 45);
   dataStore = DataStore(GetId());
   params.Parse(t_node);
}

void DefaultController::ControlStep() {

   walk.move();
   
   CColor color = getColor();
   if (color != CColor::ORANGE)
   {
      dataStore.Add(step, color);
   }

   CColor majorCalor = dataStore.GetMajorColor(params.rejectVotes);
   leds->SetColor(0, majorCalor);
   leds->SetColor(1, majorCalor);
   leds->SetColor(2, majorCalor);

   step++;
}

CColor DefaultController::getColor() {
   double reading = groundSensors->GetReading(1);
   if (reading == 0) return CColor::BLACK;
   else if (reading == 1) return CColor::WHITE;
   else if (reading > 0 && reading < 0.1) return CColor::BLUE;
   else if (reading > 0.1 && reading < 0.2) return CColor::RED;
   else if ( reading > 0.2 && reading < 0.6) return CColor::GREEN;
   else return CColor::ORANGE;
}

DataStore& DefaultController::getDataStore() {
   return dataStore;
}

double DefaultController::getCommunicationRange() {
    return params.communicationRange;
}

bool DefaultController::isByz() {
   return false;
}

REGISTER_CONTROLLER(DefaultController, "default_controller")
