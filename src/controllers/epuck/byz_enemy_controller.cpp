#include "byz_enemy_controller.h"

void ByzEnemyController::Init(TConfigurationNode& t_node) {
    wheels = GetActuator<CCI_EPuckWheelsActuator>("epuck_wheels");
    leds = GetActuator<CCI_EPuckRGBLEDsActuator>("epuck_rgb_leds");
    
    proximitySensors = GetSensor<CCI_EPuckProximitySensor>("epuck_proximity");
    groundSensors = GetSensor<CCI_EPuckGroundSensor>("epuck_ground");
    
    randomGenerator = CRandom::CreateRNG("argos");

    walk = RandomWalk(wheels, proximitySensors, randomGenerator, 400, 45);
    dataStore = DataStore(GetId());
    params.Parse(t_node);
}

void ByzEnemyController::ControlStep() {

    walk.move();

    CColor color = getColor();
    if (color != CColor::ORANGE)
    {
        dataStore.Add(step, color);
    } else {
        double reading = groundSensors->GetReading(1);
        if (reading == 0) color = CColor::BLACK;
        else if (reading == 1) color = CColor::WHITE;
        else if (reading > 0 && reading < 0.1) color = CColor::BLUE;
        else if (reading > 0.1 && reading < 0.2) color = CColor::RED;
        else if ( reading > 0.2 && reading < 0.6) color = CColor::GREEN;
        else color = CColor::ORANGE;
        dataStore.Add(step, color);
    }


    CColor majorCalor = dataStore.GetMajorColor(params.rejectVotes);
    leds->SetColor(0, majorCalor);
    leds->SetColor(1, majorCalor);
    leds->SetColor(2, majorCalor);

    step++;
}

DataStore& ByzEnemyController::getDataStore() {
   return dataStore;
}

double ByzEnemyController::getCommunicationRange() {
    return params.communicationRange;
}

CColor ByzEnemyController::getColor() {
    return dataStore.GetMinorColor();
}

REGISTER_CONTROLLER(ByzEnemyController, "byz_enemy_controller")