#ifndef DATA_STORE_H
#define DATA_STORE_H

#include <string>
#include <map>
#include <argos3/core/utility/datatypes/color.h>
#include "color.h"

using namespace std;
using namespace argos;

class DataStore
{
public:
    struct DataUnit
    {
        public:
        map<UColor, unsigned int, UColor::cmp> colors;
        unsigned int updateStep = 1;
    };

    string ownId = "NULL";
    map<string, DataUnit> data;
    UColor majorColor;

    DataStore() {}
    DataStore(string ownId) {
        this->ownId = ownId;
    }

    virtual void Refresh(string id);
    virtual void Add(unsigned int step, CColor color);
    virtual void Add(map<string, DataUnit> anotherData);
    virtual CColor GetMajorColor(bool withReject);
    virtual CColor GetMinorColor();

    virtual double GetQuality();
    virtual double GetOwnQuality();
    virtual double GetQuality(string key);
    virtual double GetConfidence(DataUnit dataUnit, int colorsNumber);
};

#endif