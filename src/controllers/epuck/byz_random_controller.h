#ifndef BYZ_RANDOM_CONTROLLER_H
#define BYZ_RANDOM_CONTROLLER_H

#include "idsrobot.h"
#include "data_store.h"
#include "random_walk.h"

#include <argos3/core/control_interface/ci_controller.h>
#include <argos3/core/control_interface/ci_controller.h>
#include <argos3/plugins/robots/e-puck/control_interface/ci_epuck_wheels_actuator.h>
#include <argos3/plugins/robots/e-puck/control_interface/ci_epuck_rgb_leds_actuator.h>
#include <argos3/plugins/robots/e-puck/control_interface/ci_epuck_proximity_sensor.h>
#include <argos3/plugins/robots/e-puck/control_interface/ci_epuck_ground_sensor.h>
#include <argos3/core/utility/math/rng.h>

class ByzRandomController : public CCI_Controller, public IDSRobot {

private:
    struct Params {
        int colorsNumber; // Количество цветов (от 1 до 5 включительно)
        double communicationRange;
        int colorsTimer;
        bool rejectVotes;

        void Parse(TConfigurationNode& t_node) {
            try {
                GetNodeAttributeOrDefault(t_node, "colorsNumber", colorsNumber, 5);
                GetNodeAttributeOrDefault(t_node, "communicationRange", communicationRange, 0.22);
                GetNodeAttributeOrDefault(t_node, "colorsTimer", colorsTimer, 0);
                GetNodeAttributeOrDefault(t_node, "rejectVotes", rejectVotes, false);
            } catch(CARGoSException& ex) {
                THROW_ARGOSEXCEPTION_NESTED("Error initializing arena parameters.", ex);
            }
        }
    };


    UInt16 step = 1;

    CCI_EPuckWheelsActuator* wheels; // Управление шасси
    CCI_EPuckRGBLEDsActuator* leds; // Управление световыми индикаторами
    CCI_EPuckProximitySensor* proximitySensors; // Датчики приближения
    CCI_EPuckGroundSensor* groundSensors; // Датчики цвета пола
    CRandom::CRNG* randomGenerator; // Встроенный в робота рандомизатор

    RandomWalk walk;
    DataStore dataStore;
    Params params;

    vector<double> colorProbabilities;

public:
    ByzRandomController() {};
    ~ByzRandomController() {};
    virtual void Init(TConfigurationNode& t_node);
    virtual void ControlStep();

    virtual DataStore& getDataStore();
    virtual double getCommunicationRange();

private:
    void initColorProbabilities();
    CColor getColor();


};

#endif