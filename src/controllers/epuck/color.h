#ifndef UCOLOR_H
#define UCOLOR_H

#include <argos3/core/utility/datatypes/color.h>

using namespace argos;

class UColor 
{
public:
    CColor color;

    UColor(CColor color) {
        this->color = color;
    }

    UColor() {
        this->color = CColor::ORANGE;
    }

    class cmp {
    public:
        bool operator()(const UColor &a, const UColor &b) const
        {
            return a.color.ToGrayScale() < b.color.ToGrayScale();
        } 
    };
};

#endif