#include "random_walk.h"

RandomWalk::RandomWalk(CCI_EPuckWheelsActuator* wheels, CCI_EPuckProximitySensor* proximitySensors, CRandom::CRNG* randomGenerator, int maxStepsForWalk, int maxStepsForTurn) {
    this->wheels = wheels;
    this->proximitySensors = proximitySensors;
    this->randomGenerator = randomGenerator;

    this->maxStepsForWalk = maxStepsForWalk;
    this->maxStepsForTurn = maxStepsForTurn;
}

RandomWalk::~RandomWalk() {}

void RandomWalk::move() {

   if (moveTime <= 0) {
      if (direction == 0) 
      {
         // Если робот шел прямо, повернуть его в некоторую позицию за случайно время
         int stepsForTurn = randomGenerator->Uniform(CRange<int>(0, maxStepsForTurn));
         int directionForTurn = randomGenerator->Uniform(CRange<int>(0, 2));
         if ( directionForTurn == 0 ) direction = 1;
         else direction = 2;
         moveTime = stepsForTurn;
      }
      else 
      {
         // Робот поворачивал, теперь нужно идти прямо экспоненциальный период времени
         direction = 0;
         moveTime = randomGenerator->Exponential(maxStepsForWalk/4)*4;
      }
   }

   switch (direction)
   {
      case 0:
         wheels->SetLinearVelocity(10, 10);
         break;
      case 1:
         wheels->SetLinearVelocity(1, 10);
         break;
      case 2:
         wheels->SetLinearVelocity(10, 1);
         break;
      default:
         break;
   }
   moveTime--;
   
   const CCI_EPuckProximitySensor::TReadings& readings = proximitySensors->GetReadings();
   double ax = 0;
   double ay = 0;
   for (int i = 0; i < readings.size(); i++) {
      ax += cos(readings[i].Angle.GetValue()) * readings[i].Value;
      ay += sin(readings[i].Angle.GetValue()) * readings[i].Value;
   }
   if(readings.size() > 0) {
      ax /= readings.size();
      ay /= readings.size();
   }
   double angle = atan2(ay, ax);
   if (!(angle >= -M_PI/18 && angle <= M_PI/18 && sqrt(ax*ax + ay*ay) < 0.5f)) {
      if(angle > 0.0f) {
         wheels->SetLinearVelocity(10, 1);
      }
      else {
         wheels->SetLinearVelocity(1, 10);
      }
   }
}