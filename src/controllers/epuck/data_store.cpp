#include "data_store.h"

void DataStore::Refresh(string id)
{
    this->ownId = id;
    data[id] = DataUnit();
}

void DataStore::Add(unsigned int step, CColor color)
{
    UColor ucolor = UColor(color);
    data[ownId].colors[color]++;
    data[ownId].updateStep = step;
}

void DataStore::Add(map<string, DataUnit> anotherData)
{
    for (map<string, DataUnit>::iterator it = anotherData.begin(); it != anotherData.end(); it++)
    {
        string key = it->first;
        if (data.count(key) == 0 || data[key].updateStep < anotherData[key].updateStep)
        {
            data[key] = anotherData[key];
        }
    }
}

CColor DataStore::GetMajorColor(bool withReject)
{
    unsigned int colorsNumber = 2;
    for (map<string, DataUnit>::iterator robotData = data.begin(); robotData != data.end(); robotData++)
    {
        if (robotData->second.colors.size() > colorsNumber)
        {
            colorsNumber = robotData->second.colors.size();
        }
        
    }

    double ownConfidence = GetConfidence(data[ownId], colorsNumber);
    map<UColor, unsigned int, UColor::cmp> tempColors;
    for (map<string, DataUnit>::iterator robotData = data.begin(); robotData != data.end(); robotData++)
    {
        if (withReject)
        {
            double confidence = GetConfidence(robotData->second, colorsNumber);
            if (abs(ownConfidence - confidence) <= 0.1)
            {
                for (map<UColor, unsigned int, UColor::cmp>::iterator color = data[robotData->first].colors.begin(); color != data[robotData->first].colors.end(); color++)
                {   
                    tempColors[color->first] += data[robotData->first].colors[color->first];
                }
            } else {
                cout << ownId << ": " << ownConfidence << ";  " << robotData->first << ": " << confidence << endl;
            }
        } else {
            for (map<UColor, unsigned int, UColor::cmp>::iterator color = data[robotData->first].colors.begin(); color != data[robotData->first].colors.end(); color++)
            {   
                tempColors[color->first] += data[robotData->first].colors[color->first];
            }
        }
    }

    UColor maxColor = UColor(CColor::ORANGE);
    int max = 0;
    for (map<UColor, unsigned int, UColor::cmp>::iterator color = tempColors.begin(); color != tempColors.end(); color++)
    {
        if (color->second >= max)
        {
            max = color->second;
            maxColor = color->first;
        }
    }

    majorColor = maxColor;

    return maxColor.color;
}

CColor DataStore::GetMinorColor()
{
    map<UColor, unsigned int, UColor::cmp> tempColors;
    double currentStep = data[ownId].updateStep;

    for (map<string, DataUnit>::iterator robotData = data.begin(); robotData != data.end(); robotData++)
    {
        for (map<UColor, unsigned int, UColor::cmp>::iterator color = data[robotData->first].colors.begin(); color != data[robotData->first].colors.end(); color++)
        {   
            tempColors[color->first] += data[robotData->first].colors[color->first];
        }
    }

    UColor minColor = UColor(CColor::ORANGE);
    int min = 0xFFFF;
    for (map<UColor, unsigned int, UColor::cmp>::iterator color = tempColors.begin(); color != tempColors.end(); color++)
    {
        if (color->second < min)
        {
            min = color->second ;
            minColor = color->first;
        }
    }

    return minColor.color;
}

double DataStore::GetQuality()
{
    double black = 0;
    double white = 0;
    double currentStep = data[ownId].updateStep;

    for (map<string, DataUnit>::iterator it = data.begin(); it != data.end(); it++)
    {
        string key = it->first;
        double weight = data[key].updateStep / currentStep;
        // white += weight * data[key].white;
        // black += weight * data[key].black;
    }

    if (white > black)
    {
        return black / white;
    }
    return white / black;
}

double DataStore::GetOwnQuality()
{
    double black = 0;
    double white = 0;
    double currentStep = data[ownId].updateStep;

    string key = ownId;
    double weight = data[key].updateStep / currentStep;
    // white += weight * data[key].white;
    // black += weight * data[key].black;

    if (white > black)
    {
        return black / white;
    }
    return white / black;
}

double DataStore::GetQuality(string key)
{
    double black = 0;
    double white = 0;
    double currentStep = data[ownId].updateStep;

    double weight = data[key].updateStep / currentStep;
    // white += weight * data[key].white;
    // black += weight * data[key].black;

    if (white > black)
    {
        return black / white;
    }
    return white / black;
}

double DataStore::GetConfidence(DataUnit dataUnit, int colorsNumber)
{
    double max = 0;
    double sum = 0;
    for (map<UColor, unsigned int, UColor::cmp>::iterator color = dataUnit.colors.begin(); color != dataUnit.colors.end(); color++)
    {   
        if (color->second > max) max = color->second;
        sum += color->second;
    }

    if (max == 0)
    {
        return 0;
    }
    
    double result = (sum - max) / max / (colorsNumber - 1);
    return result;
}