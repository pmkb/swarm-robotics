#include "byz_sectarian_controller.h"

void ByzSectarianController::Init(TConfigurationNode& t_node) {
    wheels = GetActuator<CCI_EPuckWheelsActuator>("epuck_wheels");
    leds = GetActuator<CCI_EPuckRGBLEDsActuator>("epuck_rgb_leds");
    
    proximitySensors = GetSensor<CCI_EPuckProximitySensor>("epuck_proximity");
    groundSensors = GetSensor<CCI_EPuckGroundSensor>("epuck_ground");
    
    randomGenerator = CRandom::CreateRNG("argos");

    walk = RandomWalk(wheels, proximitySensors, randomGenerator, 400, 45);
    dataStore = DataStore(GetId());
    params.Parse(t_node);
}

void ByzSectarianController::ControlStep() {

    walk.move();

    CColor color = getColor();
    if (color != CColor::PURPLE)
    {
        dataStore.Add(step, color);
    }


    CColor majorCalor = dataStore.GetMajorColor(params.rejectVotes);
    leds->SetColor(0, majorCalor);
    leds->SetColor(1, majorCalor);
    leds->SetColor(2, majorCalor);

    step++;
}

DataStore& ByzSectarianController::getDataStore() {
   return dataStore;
}

double ByzSectarianController::getCommunicationRange() {
    return params.communicationRange;
}

CColor ByzSectarianController::getColor() {
    return CColor::BLACK;
}

REGISTER_CONTROLLER(ByzSectarianController, "byz_sectarian_controller")