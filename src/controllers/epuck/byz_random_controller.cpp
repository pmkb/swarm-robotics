#include "byz_random_controller.h"

#include <vector>

void ByzRandomController::Init(TConfigurationNode& t_node) {
    wheels = GetActuator<CCI_EPuckWheelsActuator>("epuck_wheels");
    leds = GetActuator<CCI_EPuckRGBLEDsActuator>("epuck_rgb_leds");
    
    proximitySensors = GetSensor<CCI_EPuckProximitySensor>("epuck_proximity");
    groundSensors = GetSensor<CCI_EPuckGroundSensor>("epuck_ground");
    
    randomGenerator = CRandom::CreateRNG("argos");

    walk = RandomWalk(wheels, proximitySensors, randomGenerator, 400, 45);
    dataStore = DataStore(GetId());
    params.Parse(t_node);

    initColorProbabilities();
}

void ByzRandomController::ControlStep() {

    if (params.colorsTimer != 0 && step % params.colorsTimer == 0)
    {
        initColorProbabilities();
    }

    walk.move();

    CColor color = getColor();

    dataStore.Add(step, color);

    CColor majorCalor = dataStore.GetMajorColor(params.rejectVotes);
    leds->SetColor(0, majorCalor);
    leds->SetColor(1, majorCalor);
    leds->SetColor(2, majorCalor);

    step++;
}

DataStore& ByzRandomController::getDataStore() {
   return dataStore;
}

double ByzRandomController::getCommunicationRange() {
    return params.communicationRange;
}

void ByzRandomController::initColorProbabilities() {
    colorProbabilities.clear();
    for (int i = 0; i < params.colorsNumber; i++) {
        double prev = 0;
        if (i > 0) prev = colorProbabilities[i-1];
        //colorProbabilities.push_back(prev + randomGenerator->Uniform(CRange<double>(0, 1.0)));
        colorProbabilities.push_back(prev + 1);
    }
}

CColor ByzRandomController::getColor() {
    double colorPicker = randomGenerator->Uniform(CRange<double>(0, colorProbabilities.back()));
    for (int i = 0; i < params.colorsNumber; i++) {
        if (colorPicker <= colorProbabilities[i])
        {
            if (i==0) return CColor::WHITE;
            else if (i==1) return CColor::BLACK;
            else if (i==2) return CColor::RED;
            else if (i==3) return CColor::GREEN;
            else if (i==4) return CColor::BLUE;
            else return CColor::ORANGE;
        }
    }
}

REGISTER_CONTROLLER(ByzRandomController, "byz_random_controller")