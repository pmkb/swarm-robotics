#ifndef IDSROBOT_H
#define IDSROBOT_H

#include "data_store.h"

class IDSRobot {
public:
    virtual DataStore& getDataStore() = 0;
    virtual double getCommunicationRange() = 0;
    virtual bool isByz() {
        return true;
    }
};

#endif