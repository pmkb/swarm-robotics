#ifndef ACCUMULATION_METHOD_LOOP_H
#define ACCUMULATION_METHOD_LOOP_H

#include <argos3/core/simulator/loop_functions.h>
#include <argos3/core/simulator/entity/floor_entity.h>
#include <argos3/core/simulator/simulator.h>
#include <argos3/core/utility/math/rng.h>
#include <argos3/core/utility/logging/argos_log.h>
#include <argos3/core/utility/configuration/argos_configuration.h>
#include <argos3/plugins/robots/e-puck/simulator/epuck_entity.h>
#include <argos3/plugins/simulator/entities/box_entity.h>
#include <argos3/plugins/simulator/entities/cylinder_entity.h>

#include "controllers/epuck/idsrobot.h"
#include "controllers/epuck/data_store.h"

#include "controllers/epuck/default_controller.h"

#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <stddef.h>
#include <list>
#include <unistd.h>
#include <sstream>
#include <math.h>
#include <time.h>
#include <random>

using namespace argos;
using namespace std;

static const string ROBOTS_PARAMS_NAME = "robots_params";

class MainLoop : public CLoopFunctions {
public:

    struct Params {
        double whitePercent;
        double blackPercent;
        double redPercent;
        double greenPercent;
        double bluePercent;
        string floorType; // [random_shuffle, random_stripes, chess_field]
        string fullLog; // [observer, robot]
        int consensusMinStep;

        void Parse(TConfigurationNode& t_node) {
            try {
                static const string DEFAULT_FLOOR_TYPE = "random_shuffle";
                static const string DEFAULT_FULL_LOG = "NONE";
        
                GetNodeAttributeOrDefault(t_node, "whitePercent", whitePercent, 0.0);
                GetNodeAttributeOrDefault(t_node, "blackPercent", blackPercent, 100.0);
                GetNodeAttributeOrDefault(t_node, "redPercent", redPercent, 0.0);
                GetNodeAttributeOrDefault(t_node, "greenPercent", greenPercent, 0.0);
                GetNodeAttributeOrDefault(t_node, "bluePercent", bluePercent, 0.0);
                GetNodeAttributeOrDefault(t_node, "floorType", floorType, DEFAULT_FLOOR_TYPE);
                GetNodeAttributeOrDefault(t_node, "fullLog", fullLog, DEFAULT_FULL_LOG);
                GetNodeAttributeOrDefault(t_node, "consensusMinStep", consensusMinStep, 1);

                whitePercent /= 100.0;
                blackPercent /= 100.0;
                redPercent /= 100.0;
                greenPercent /= 100.0;
                bluePercent /= 100.0;

                if (whitePercent < 0)   whitePercent = 0;
                if (blackPercent < 0)   blackPercent = 0;
                if (redPercent < 0)     redPercent = 0;
                if (greenPercent < 0)   greenPercent = 0;
                if (bluePercent < 0)    bluePercent = 0;

                int colors = 0;
                if (whitePercent != 0)  colors++;
                if (blackPercent != 0)  colors++;
                if (redPercent != 0)    colors++;
                if (greenPercent != 0)  colors++;
                if (bluePercent != 0)   colors++;

                double percent = whitePercent + blackPercent + redPercent + greenPercent + bluePercent;
                double correction = (1 - percent) / colors;

                if (whitePercent != 0)  whitePercent += correction;
                if (blackPercent != 0)  blackPercent += correction;
                if (redPercent != 0)    redPercent += correction;
                if (greenPercent != 0)  greenPercent += correction;
                if (bluePercent != 0)   bluePercent += correction;

            } catch(CARGoSException& ex) {
                THROW_ARGOSEXCEPTION_NESTED("Error initializing arena parameters.", ex);
            }
        }
    };

public:

    MainLoop() {};
    virtual ~MainLoop() {};
    virtual void Init(TConfigurationNode& t_tree);
    virtual void PostStep();
    virtual void PreStep();
    virtual bool IsExperimentFinished();
    virtual void PostExperiment();
    virtual CColor GetFloorColor(const CVector2& c_pos_on_floor); // Определяет цвет ячейки

private:
    CRandom::CRNG* randomGenerator;
    Params params;   

    static const int CELLS_IN_METER = 10;
    int xArenaCells;  // Размер арены по оси X в клетках
    int yArenaCells;  // Размер арены по оси Y в клетках
    vector<vector<CColor>> floorColorField;  // Матрица цветового покрытия пола

    vector<CEPuckEntity*> robots;  // Вектор указателей на объекты роботов
    vector<IDSRobot*> controllers;  // Вектор указателей на контролеры роботов

private:
    bool consensusReached();
    pair<UColor, int> getMaxColor();
    void InitRobots();
    vector<vector<CColor>> GenerateFloorColorField(int xArenaCells, int yArenaCells, Params params);
    vector<vector<CColor>> GenerateChessField(int xArenaCells, int yArenaCells);

};

#endif
