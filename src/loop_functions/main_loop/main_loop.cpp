#include "main_loop.h"

void MainLoop::Init(TConfigurationNode& t_node) {
    randomGenerator = CRandom::CreateRNG("argos");
    params.Parse(GetNode(t_node, "params"));

    xArenaCells = GetSpace().GetArenaSize().GetX() * CELLS_IN_METER;
    yArenaCells = GetSpace().GetArenaSize().GetY() * CELLS_IN_METER;

    if (params.floorType == "chess_field")
    {
        floorColorField = GenerateChessField(xArenaCells, yArenaCells);
    } else {
        floorColorField = GenerateFloorColorField(xArenaCells, yArenaCells, params);
    }

    InitRobots();

    if (params.fullLog == "observer" || params.fullLog == "robot") {
        string resultFileName = "./temp/full_log.csv";
        ofstream resultOfstream;
        resultOfstream.open(resultFileName, std::fstream::ate);

        UColor colorsOrder[] = {UColor(CColor::WHITE), UColor(CColor::BLACK), UColor(CColor::RED), UColor(CColor::GREEN), UColor(CColor::BLUE)};
        
        resultOfstream << "step" << ";"; 
        if (params.fullLog == "robot") {
            resultOfstream << "robot_id" << ";"; 
        }
        for (int i = 0; i < robots.size(); i++) {
            for (int j = 0; j < 5; j++) {
                string color = "";

                if (colorsOrder[j].color == CColor::WHITE) {
                    color = "WHITE";
                } else if (colorsOrder[j].color == CColor::BLACK) {
                    color = "BLACK";
                } else if (colorsOrder[j].color == CColor::RED) {
                    color = "RED";
                } else if (colorsOrder[j].color == CColor::GREEN) {
                    color = "GREEN";
                } else if (colorsOrder[j].color == CColor::BLUE) {
                    color = "BLUE";
                } 

                resultOfstream << robots[i]->GetId() << "_" << color << ";"; 
            }
            
        }
        resultOfstream << endl;
        resultOfstream.close();
    }
}

void MainLoop::PreStep()
{
    // Замена радиоканала ): 
    CVector3 positions[robots.size()];
    for (int i = 0; i < robots.size(); i++) {
        positions[i] = robots[i]->GetEmbodiedEntity().GetOriginAnchor().Position;
    }
    for (int a = 0; a < robots.size(); a++) {
        for (int b = a+1; b < robots.size(); b++) {
            if (Distance(positions[a], positions[b]) < controllers[a]->getCommunicationRange()) {
                controllers[b]->getDataStore().Add(controllers[a]->getDataStore().data);
            }
            if (Distance(positions[a], positions[b]) < controllers[b]->getCommunicationRange())
            {
                controllers[a]->getDataStore().Add(controllers[b]->getDataStore().data);
            }
        }
    }
}

void MainLoop::PostStep()
{
    if (params.fullLog == "observer" || params.fullLog == "robot") {
        string resultFileName = "./temp/full_log.csv";
        ofstream resultOfstream;
        resultOfstream.open(resultFileName, std::fstream::out | std::fstream::app);
        int step = GetSimulator().GetSpace().GetSimulationClock();

        UColor colorsOrder[] = {UColor(CColor::WHITE), UColor(CColor::BLACK), UColor(CColor::RED), UColor(CColor::GREEN), UColor(CColor::BLUE)};

        if (params.fullLog == "observer") {
            resultOfstream << step << ";";
            for (int i = 0; i < robots.size(); i++) {
                DataStore store = controllers[i]->getDataStore();
                string ownId = store.ownId;

                for (int j = 0; j < 5; j++) {
                    DataStore::DataUnit buf = store.data[ownId];
                    resultOfstream << buf.colors[colorsOrder[j]] << ";";
                }
            }
            resultOfstream << endl;
        } else if (params.fullLog == "robot") {
            for (int i = 0; i < robots.size(); i++) {
                DataStore store = controllers[i]->getDataStore();
                string ownId = store.ownId;
                resultOfstream  << step << ";";
                resultOfstream  << ownId << ";";
                
                for (int k = 0; k < robots.size(); k++) {
                    string id = robots[k]->GetId();

                    for (int j = 0; j < 5; j++) {
                        DataStore::DataUnit buf = store.data[id];
                        resultOfstream << buf.colors[colorsOrder[j]] << ";";
                    }
                }
                resultOfstream << endl;
            }
        }



        //resultOfstream << endl;
        resultOfstream.close();
    }
}

void MainLoop::InitRobots() {
    // Создаем массив указателей на контролеры роботов
    robots.clear();
    controllers.clear();
    CSpace::TMapPerType& m_cEpuck = GetSpace().GetEntitiesByType("epuck");
    for (CSpace::TMapPerType::iterator it = m_cEpuck.begin(); it != m_cEpuck.end(); ++it) {
        CEPuckEntity &cEpuck = *any_cast<CEPuckEntity*>(it->second);
        
        IDSRobot *controller = dynamic_cast<IDSRobot*>(&cEpuck.GetControllableEntity().GetController());
        if (controller) 
        {
            robots.push_back(&cEpuck);
            controllers.push_back(controller);
        }
    }
}

bool MainLoop::IsExperimentFinished() {
    if (GetSimulator().GetSpace().GetSimulationClock() <= params.consensusMinStep)
        return false;

    if (consensusReached())
    {
        return true;
    } else {
        return false;
    }
    

    return false;
}

bool MainLoop::consensusReached() {
    pair<UColor, int> maxColor = getMaxColor();
    if (maxColor.first.color == CColor::ORANGE) return false;

    //int consensusNumber = robots.size() * 80.0 / 100.0;
    double consensusNumber = 0;

    for (auto controller : controllers)
    {
        if (controller->isByz() != true)
        {
            consensusNumber++;
        }
    }

    //cout << consensusNumber << " ";
    //consensusNumber = consensusNumber * 80.0 / 100.0;
    //cout << consensusNumber << endl; 
    

    if (maxColor.second >= consensusNumber) {
        cout << maxColor.first.color << " - " << maxColor.second << endl;
        return true;
    } else {
        return false;
    }
}

void MainLoop::PostExperiment() {
    int step = GetSimulator().GetSpace().GetSimulationClock();
    int seed = GetSimulator().GetRandomSeed();
    pair<UColor, int> maxColor = getMaxColor();

    string resultFileName = "./temp/result.csv";
    ofstream resultOfstream;
    resultOfstream.open(resultFileName);
    resultOfstream  << step << ";"
                    << seed << ";"
                    << maxColor.first.color << ";"
                    << maxColor.second << ";"
                    << consensusReached() << ";"
                    << endl;
    resultOfstream.close();
}

CColor MainLoop::GetFloorColor(const CVector2& c_pos_on_floor) {
    int x = c_pos_on_floor.GetX() * CELLS_IN_METER;
    int y = c_pos_on_floor.GetY() * CELLS_IN_METER;

    if (x >= 0 && x < xArenaCells && y >= 0 && y < yArenaCells) {
        return floorColorField[x][y];
    }

    return CColor::YELLOW;
}

pair<UColor, int> MainLoop::getMaxColor() {
    map<UColor, unsigned int, UColor::cmp> colors;
    for (auto controller : controllers)
    {
        colors[controller->getDataStore().majorColor]++;
    }

    UColor maxColor;
    int maxColorCount = 0;
    for (map<UColor, unsigned int, UColor::cmp>::iterator it = colors.begin(); it != colors.end(); it++)
    {
        UColor color = it->first;
        int count = it->second;
        if (count > maxColorCount) {
            maxColor = color;
            maxColorCount = count;
        }
    }

    return pair(maxColor, maxColorCount);
}

vector<vector<CColor>> MainLoop::GenerateFloorColorField(int xArenaCells, int yArenaCells, Params params) 
{
    int cellsCount = xArenaCells * yArenaCells;
    int white = cellsCount * params.whitePercent; 
    int black = cellsCount * params.blackPercent; 
    int red = cellsCount * params.redPercent; 
    int green = cellsCount * params.greenPercent; 
    int blue = cellsCount * params.bluePercent; 

    LOG << "[INFO] CELS TOTAL=" << cellsCount << ", " << 100<< "%" << endl;
    LOG << "[INFO] CELS WHITE=" << white << ", " << 100.0*white/cellsCount << "%" << endl;
    LOG << "[INFO] CELS BLACK=" << black << ", " << 100.0*black/cellsCount << "%" << endl;
    LOG << "[INFO] CELS RED=" << red << ", " << 100.0*red/cellsCount << "%" << endl;
    LOG << "[INFO] CELS GREEN=" << green << ", " << 100.0*green/cellsCount << "%" << endl;
    LOG << "[INFO] CELS BLUE=" << blue << ", " << 100.0*blue/cellsCount  << "%" << endl;

    vector<CColor> buf(cellsCount, CColor::ORANGE);
    int x = 0;
    for(int i = 0; i < white; i++, x++) {
        buf[x] = CColor::WHITE;
    }
    for(int i = 0; i < black; i++, x++) {
        buf[x] = CColor::BLACK;
    }
    for(int i = 0; i < red; i++, x++) {
        buf[x] = CColor(170, 0, 0);
    }
    for(int i = 0; i < green; i++, x++) {
        buf[x] = CColor(0, 170, 0);
    }
    for(int i = 0; i < blue; i++, x++) {
        buf[x] = CColor(0, 0, 170);
    }

    if (params.floorType == "random_shuffle") {
        std::shuffle(buf.begin(), buf.end(), std::default_random_engine(randomGenerator->GetSeed()));
    }

    vector<vector<CColor>> floorColorField(yArenaCells, vector<CColor>(xArenaCells, CColor::ORANGE));
    int ptrToBuf = 0;
    for (int y = 0; y < floorColorField.size(); y++) {
        for (int x = 0; x < xArenaCells; x++, ptrToBuf++) {
            floorColorField[y][x] = buf[ptrToBuf];
        }
    }

    return floorColorField;
}

vector<vector<CColor>> MainLoop::GenerateChessField(int xArenaCells, int yArenaCells) 
{
    vector<vector<CColor>> floorColorField(yArenaCells, vector<CColor>(xArenaCells, CColor::ORANGE));
    bool color = true;
    for (int y = 0; y < floorColorField.size(); y++) {
        for (int x = 0; x < xArenaCells; x++) {
            if (color == true) {
                floorColorField[y][x] = CColor::BLACK;
            } 
            else {
                floorColorField[y][x] = CColor::WHITE;
            }
            color = !color;
        }
        color = !color;
    }

    return floorColorField;
}

REGISTER_LOOP_FUNCTIONS(MainLoop, "main_loop")
