#ifndef QT_PLUGIN_H
#define QT_PLUGIN_H

//#include "controllers/epuck_ecc.h"

#include <argos3/plugins/simulator/visualizations/qt-opengl/qtopengl_user_functions.h>
#include <argos3/core/utility/logging/argos_log.h>
#include <stdio.h>
#include <argos3/plugins/robots/e-puck/simulator/epuck_entity.h>

#include "controllers/epuck/byz_sectarian_controller.h"
#include "controllers/epuck/default_controller.h"

using namespace argos;
using namespace std;

class QtPlugin : public CQTOpenGLUserFunctions {

public:

   QtPlugin();

   virtual ~QtPlugin() {}

   void Draw(CEPuckEntity& c_entity);
   
};

#endif
