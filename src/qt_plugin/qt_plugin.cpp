#include "qt_plugin.h"

/****************************************/
/****************************************/

QtPlugin::QtPlugin() {
    RegisterUserFunction<QtPlugin,CEPuckEntity>(&QtPlugin::Draw);
}

/****************************************/
/****************************************/

void QtPlugin::Draw(CEPuckEntity& c_entity) {
    CCI_Controller& controller = c_entity.GetControllableEntity().GetController();
    //ByzSectarianController& controller = dynamic_cast<ByzSectarianController&>(c_entity.GetControllableEntity().GetController());
    //string text = c_entity.GetId().c_str();
    if (typeid(controller) == typeid(ByzSectarianController))
    {
        ByzSectarianController& controller = dynamic_cast<ByzSectarianController&>(c_entity.GetControllableEntity().GetController());
        double text = controller.getDataStore().GetConfidence(controller.getDataStore().data[controller.getDataStore().ownId], 2);
        
        DrawText(CVector3(0.0, 0.0, 0.1),   // position
             std::to_string(text),
             CColor::RED);
    } 
    else if (typeid(controller) == typeid(DefaultController))
    {
        DefaultController& controller = dynamic_cast<DefaultController&>(c_entity.GetControllableEntity().GetController());
        double text = controller.getDataStore().GetConfidence(controller.getDataStore().data[controller.getDataStore().ownId], 2);
        
        DrawText(CVector3(0.0, 0.0, 0.1),   // position
             std::to_string(text),
             CColor::RED);
    }

    

    
    //text += "_";
    //text += to_string(controller.GetOpinion().GetQuality());
     // text
}

/****************************************/
/****************************************/

REGISTER_QTOPENGL_USER_FUNCTIONS(QtPlugin, "qt_plugin_functions")
